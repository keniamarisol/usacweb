package usacweb.gramaticaChtml;
import java_cup.runtime.Symbol;
import java.util.ArrayList;



%%

%cupsym tabla_simbolos1
%class Scanner1
%function next_token
%type java_cup.runtime.Symbol
%cup
%public
%unicode
%line
%column
%char
%ignorecase




%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{

public static ArrayList tokensList;
public static ArrayList tokensListErrores;;
%}



metodo= {nombre}[\(][a-zA-Z]*[\)]
//path= [a-zA-Z][\:][\/]([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+ | [a-zA-Z][\:][\/][\/]([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+
//| ([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+ 
nombre= [:jletter:]+[:jletterdigit:]*
numeros=[:digit:]+
comillas = "“" | "”" | [\"] 

/* comments */
comentario = "<//-" ~ "-//>"   

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
//InputCharacter2 = [^" "\r\n\t\f<\>\/\(\)\:\/\=\"\-]
WhiteSpace     = {LineTerminator} | [ \t\f]                                                            


texto = ([^" "\"\<\>\-\;\=\r\n\t\f])+
//texto = "\"" ~ "\"" 
 
%%



/*PALABRAS RESERVADAS*/



"CHTML" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tchtml, yycolumn,yyline, new String(yytext())); }


"ENCABEZADO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tencabezado, yycolumn,yyline, new String(yytext()));}

"FIN"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tfin, yycolumn,yyline, new String(yytext()));}


"CCSS"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tccss, yycolumn,yyline, new String(yytext())); }

"CJS"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcjs, yycolumn,yyline, new String(yytext()));}

"TITULO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ttitulo, yycolumn,yyline, new String(yytext()));}

"CUERPO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcuerpo, yycolumn,yyline, new String(yytext()));}

"PANEL"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tpanel, yycolumn,yyline, new String(yytext()));}

"BOTON"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tboton, yycolumn,yyline, new String(yytext()));}

"TEXTO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ttexto, yycolumn,yyline, new String(yytext()));}

"TEXTO_A"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ttextoa, yycolumn,yyline, new String(yytext()));}

"CAJA_TEXTO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcajatexto, yycolumn,yyline, new String(yytext()));}

"IMAGEN"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.timagen, yycolumn,yyline, new String(yytext()));}

"ENLACE"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tenlace, yycolumn,yyline, new String(yytext()));}

"TABLA"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ttabla, yycolumn,yyline, new String(yytext()));}

"FIL_T"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tfil, yycolumn,yyline, new String(yytext()));}


"CB"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcb, yycolumn,yyline, new String(yytext()));}

"CT"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tct, yycolumn,yyline, new String(yytext()));}

"CAJA"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcaja, yycolumn,yyline, new String(yytext()));}



"OPCION"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.topcion, yycolumn,yyline, new String(yytext()));}

"SPINNER"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tspinner, yycolumn,yyline, new String(yytext()));}

"SALTO"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tsalto, yycolumn,yyline, new String(yytext()));}

"grupo"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tgrupo, yycolumn,yyline, new String(yytext()));}

"alto"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.talto, yycolumn,yyline, new String(yytext()));}

"ancho"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tancho, yycolumn,yyline, new String(yytext()));}

"alineado"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.talineado, yycolumn,yyline, new String(yytext()));}

"ruta"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.truta, yycolumn,yyline, new String(yytext()));}

"fondo"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tfondo, yycolumn,yyline, new String(yytext()));}

"click"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tclick, yycolumn,yyline, new String(yytext()));}

"valor"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tvalor, yycolumn,yyline, new String(yytext()));}

"id"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tid, yycolumn,yyline, new String(yytext()));}

"izquierda"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tizquierda, yycolumn,yyline, new String(yytext()));}

"derecha"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tderecha, yycolumn,yyline, new String(yytext()));}

"centrado"  {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcentrado, yycolumn,yyline, new String(yytext()));}



"<" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tabrir, yycolumn,yyline, new String(yytext()));}

">" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcerrar, yycolumn,yyline, new String(yytext()));}

"-" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tguion, yycolumn,yyline, new String(yytext()));}

";" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tpuntoycoma, yycolumn,yyline, new String(yytext()));}

"=" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tigual, yycolumn,yyline, new String(yytext()));}

/*comillas*/
{comillas} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ercomillas, yycolumn,yyline, new String(yytext()));}

/*NUMEROS*/
{numeros} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ernumbers, yycolumn,yyline, new String(yytext()));}

/*LETRAS*/
{nombre} { System.out.println("Identifier : "+yytext()); this.tokensList.add("Nombre: "+yytext());
return new Symbol(tabla_simbolos1.eridentifier , yycolumn,yyline,new String(yytext()));}

{comentario} {/*System.out.println("Comentario: "+yytext());*/ }

//{path} { /*System.out.println("path: "+yytext());*/ this.tokensList.add("Path: "+yytext());
//return new Symbol(tabla_simbolos1.erpath, yycolumn,yyline,new String(yytext()));}


{metodo} { /*System.out.println("metodo: "+yytext());*/this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos1.ermetodo, yycolumn,yyline,new String(yytext()));}

{texto} { System.out.println("texto: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos1.ertexto, yycolumn,yyline,new String(yytext()));}


//{texto_etiqueta} { System.out.println("texto_etiqueta: "+yytext()); this.tokensList.add("Texto: "+yytext());
//return new Symbol(tabla_simbolos1.ertexto_etiqueta, yycolumn,yyline,new String(yytext()));}

/* CUALQUIER OTRO */
{WhiteSpace}            { /* se ignoran  */}
.                       {this.tokensListErrores.add("Token: "+yytext());
                         System.out.println("Error léxico: "+yytext());}