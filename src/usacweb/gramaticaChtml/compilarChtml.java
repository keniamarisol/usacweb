/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usacweb.gramaticaChtml;

import java.awt.Button;
import java.io.FileReader;
import java.util.LinkedList;
import usacweb.graficar.graficarArbolCjs;
import usacweb.graficar.graficarArbolHtml;
import usacweb.graficar.nodoArbol;
import usacweb.gramaticaCjs.Parser2;
import usacweb.gramaticaCjs.Scanner2;
import usacweb.panelPagina;
import usacweb.ventanaPrincipal;

/**
 *
 * @author id_ma
 */
public class compilarChtml {

    panelPagina pag;
    int contador = 0;
    int x = 0;
    int y = 0;

    public void iniciar(nodoArbol raiz, panelPagina pag) {

        this.pag = pag;

        String nom = raiz.produccion;

        switch (nom) {
            case "CHTML":
                recorrerChtml(raiz.hijos);
                break;
        }
    }

    public void recorrerChtml(LinkedList<nodoArbol> hijos) {

        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion;

            switch (produccion) {

                case "ENCABEZADO":
                    recorrerEncabezado(hijo.hijos.get(0).hijos);
                    break;
                case "CUERPO":
                    recorrerCuerpo(hijo.hijos);
                    break;
            }
        }

    }

    public void recorrerCuerpo(LinkedList<nodoArbol> hijos) {

        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion;

            switch (produccion) {

                case "FONDO":
                    break;
                case "CONTENIDO":
                    recorrerContenido(hijo.hijos);
                    break;
            }
        }

    }

    public void recorrerEncabezado(LinkedList<nodoArbol> hijos) {

        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion;

            switch (produccion) {

                case "CJS":
                    compilarCJS(hijo.hijos.get(0).hijos.get(0).produccion);

                    break;
                case "CSS":
                    break;
                case "TITULO":
                    break;
            }
        }

    }

    public void recorrerContenido(LinkedList<nodoArbol> hijos) {

        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion;

            switch (produccion) {

                case "PANEL":
                    recorrerCuerpo(hijo.hijos);
                    break;
                case "IMAGEN":
                    break;
                case "BOTON":
                    crearBoton(hijo.hijos);
                    break;
                case "TEXTO":
                    break;
                case "ENLACE":
                    break;
                case "TEXTO_A":
                    break;
                case "CAJA_TEXTO":
                    break;
                case "CAJA_OPCIONES":
                    break;
                case "SALTO":
                    break;
            }
        }

    }

    public void crearBoton(LinkedList<nodoArbol> hijos) {

        String grupo = "",
                alto = "50",
                ancho = "100",
                alineado = "left",
                ruta = "",
                click = "",
                valor = "",
                parrafo = "button" + contador;

        String id = "button" + contador;
        contador++;
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion;

            switch (produccion) {

                case "ELEMENTOS":
                    if (hijo.hijos.size() > 0) {

                        for (nodoArbol hij : hijo.hijos) {
                            String prod = hij.produccion;

                            switch (prod) {

                                case "ID":
                                    id = hij.hijos.get(0).produccion;
                                    break;
                                case "GRUPO":
                                    grupo = hij.hijos.get(0).produccion;
                                    break;
                                case "ALTO":
                                    alto = hij.hijos.get(0).produccion;
                                    break;
                                case "ANCHO":
                                    ancho = hij.hijos.get(0).produccion;
                                    break;
                                case "ALINEADO":
                                    alineado = hij.hijos.get(0).produccion;
                                    break;
                                case "RUTA":
                                    ruta = hij.hijos.get(0).produccion;
                                    break;
                                case "CLICK":
                                    click = hij.hijos.get(0).produccion;
                                    break;
                                case "VALOR":
                                    valor = hij.hijos.get(0).produccion;
                                    break;
                            }
                        }
                    }

                    break;
                case "PARRAFO":
                    parrafo = hijos.get(1).hijos.get(0).produccion;
                    break;

            }
        }

        try {
            Button boton = new Button();
            boton.setLabel(parrafo);
            boton.setSize(Integer.valueOf(ancho), Integer.valueOf(alto));
            boton.setName(id);
            //  x += Integer.valueOf(ancho);
            //  y += Integer.valueOf(alto);
            // boton.setLocation(x, y);
            pag.addComponent(boton);

        } catch (Exception e) {
            System.out.println("Error al agregar boton");
        }

        // boton.setLocation(0, 0);
    }

    public void compilarCJS(String ruta) {
        System.out.println("------------------------------Compilando CJS------------------------------");
        try {
            // TODO add your handling code here:
            ruta = ruta.replace("\\", "/");
            String rutaCJS = ruta;
            FileReader fr = new FileReader(rutaCJS);
            Scanner2 lex = new Scanner2(fr);
            Parser2 miParser = new Parser2(lex);
            try {
                miParser.parse();
                System.out.println("si compilo parser CJS");
                graficarArbolCjs graficar = new graficarArbolCjs();
                graficar.grafico(miParser.nodoRaiz);
                // compilarChtml compilar = new compilarChtml();
                //   compilar.iniciar(miParser.nodoRaiz, this);
            } catch (Exception ex) {
                System.out.println("Error al parsear/graficar CJS");
            }

            //  new Parser1(new Scanner1(new FileReader(rutaCHTML))).parse();
            //   System.out.println(lex.tokensList.toString());
            System.out.println("si entra al try CJS");
        } catch (Exception ex) {
            System.out.println("Error al Compilar CJS");
        }
    }

}
