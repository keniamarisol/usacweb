package usacweb.gramaticaCjs;
import java_cup.runtime.Symbol;
import java.util.ArrayList;



%%

%cupsym tabla_simbolos2
%class Scanner2
%function next_token
%type java_cup.runtime.Symbol
%cup
%public
%unicode
%line
%column
%char
%ignorecase


%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{

public static ArrayList tokensList;
public static ArrayList tokensListErrores;;
%}



nombre= [:jletter:]+[:jletterdigit:]*
funcion= {nombre}[\(]({nombre}[\,]?)*[\)]
path= [a-zA-Z][\:][\/]([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+ | [a-zA-Z][\:][\/][\/]([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+
| ([a-zA-Z0-9\.\_]+[\/])+[a-zA-Z0-9\.\_]+ 

date = (([1-2][0-9])|([0]?[1-9])|([3][0-1]))[\/](([1][0-2])|([0]?[1-9]))[\/][0-9]{4}
datetime = {date}" "([0-1][0-9]|2[0-3])":"[0-5][0-9]":"[0-5][0-9]
numerico =[:digit:]+[.]?[:digit:]*
integer =[:digit:]+

/* comments */
comentario = {TraditionalComment}
TraditionalComment   = "'/" [^\/] ~"/'" | "'/" + "/'"
EndOfLineComment     = "'" {InputCharacter}* {LineTerminator}? 

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n\']
WhiteSpace     = {LineTerminator} | [ \t\f]    

string = "\"" ~ "\"" 
comillas = "“" | "”" | [\"]

                                                       

%%


/*PALABRAS RESERVADAS*/


"DimV" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdimv, yycolumn,yyline, new String(yytext())); }

"True" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.ttrue, yycolumn,yyline, new String(yytext())); }

"False" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tfalse, yycolumn,yyline, new String(yytext())); }

"Conteo" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tconteo, yycolumn,yyline, new String(yytext())); }

"aTexto()" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tatexto, yycolumn,yyline, new String(yytext())); }

"Si" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tsi, yycolumn,yyline, new String(yytext())); }

"Sino" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tsino, yycolumn,yyline, new String(yytext())); }

"Selecciona" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tselecciona, yycolumn,yyline, new String(yytext())); }

"Caso" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcaso, yycolumn,yyline, new String(yytext())); }

"Defecto" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdefecto, yycolumn,yyline, new String(yytext())); }

"Para" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tpara, yycolumn,yyline, new String(yytext())); }

"mientras" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmientras, yycolumn,yyline, new String(yytext())); }

"detener" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdetener, yycolumn,yyline, new String(yytext())); }

"imprimir" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.timprimir, yycolumn,yyline, new String(yytext())); }

"funcion" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tfuncion, yycolumn,yyline, new String(yytext())); }

"retornar" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tretornar, yycolumn,yyline, new String(yytext())); }

"mensaje" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmensaje, yycolumn,yyline, new String(yytext())); }

"Documento" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdocumento, yycolumn,yyline, new String(yytext())); }

"Listo" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tlisto, yycolumn,yyline, new String(yytext())); }

"Modificado" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmodificado, yycolumn,yyline, new String(yytext())); }

"Cliqueado" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcliqueado, yycolumn,yyline, new String(yytext())); }

"Observador" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tobservador, yycolumn,yyline, new String(yytext())); }

"Obtener" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tobtener, yycolumn,yyline, new String(yytext())); }

"setElemento" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tsetelemento, yycolumn,yyline, new String(yytext())); }

"." {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tpunto, yycolumn,yyline, new String(yytext())); }

"," {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcoma, yycolumn,yyline, new String(yytext())); }

"{" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tllavei, yycolumn,yyline, new String(yytext())); }

"}" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tllaved, yycolumn,yyline, new String(yytext())); }

":" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdospuntos, yycolumn,yyline, new String(yytext()));}

"'" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcomillasimple, yycolumn,yyline, new String(yytext()));}

";" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tpuntoycoma, yycolumn,yyline, new String(yytext()));}

"==" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tigual, yycolumn,yyline, new String(yytext()));}

"<" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmenor, yycolumn,yyline, new String(yytext())); }

">" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmayor, yycolumn,yyline, new String(yytext())); }

"<=" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmenorigual, yycolumn,yyline, new String(yytext())); }

">=" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmayorigual, yycolumn,yyline, new String(yytext())); }

"||" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tor, yycolumn,yyline, new String(yytext())); }

"&&" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tand, yycolumn,yyline, new String(yytext())); }

"!" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tnot, yycolumn,yyline, new String(yytext())); }


"!=" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdiferente, yycolumn,yyline, new String(yytext())); }

"+" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmas, yycolumn,yyline, new String(yytext())); }

"-" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmenos, yycolumn,yyline, new String(yytext()));}

"++" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tincremento, yycolumn,yyline, new String(yytext())); }

"--" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdecremento, yycolumn,yyline, new String(yytext()));}

"*" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tpor, yycolumn,yyline, new String(yytext())); }

"/" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdivision, yycolumn,yyline, new String(yytext())); }

"%" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tmodulo, yycolumn,yyline, new String(yytext())); }

"^" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.televado, yycolumn,yyline, new String(yytext())); }

"(" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tparentesisi, yycolumn,yyline, new String(yytext())); }

")" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tparentesisd, yycolumn,yyline, new String(yytext())); }

/*comillas*/
{comillas} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.ercomillas, yycolumn,yyline, new String(yytext()));}

/*LETRAS*/
{nombre} {System.out.println("Identifier : "+yytext()); this.tokensList.add("Nombre: "+yytext());
return new Symbol(tabla_simbolos2.eridentifier , yycolumn,yyline,new String(yytext()));}

{date} { System.out.println("date: "+yytext());this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.erdate, yycolumn,yyline,new String(yytext()));}

{datetime} {System.out.println("datetime: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.erdatetime, yycolumn,yyline,new String(yytext()));}

{path} { System.out.println("path: "+yytext()); this.tokensList.add("Path: "+yytext());
return new Symbol(tabla_simbolos2.erpath, yycolumn,yyline,new String(yytext()));}

{string} { System.out.println("string: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.erstring, yycolumn,yyline,new String(yytext()));}

{integer} { System.out.println("integer: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.erinteger, yycolumn,yyline,new String(yytext()));}

{numerico} { System.out.println("numerico: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.ernumerico, yycolumn,yyline,new String(yytext()));}

{funcion} { System.out.println("funcion: "+yytext()); this.tokensList.add("Texto: "+yytext());
return new Symbol(tabla_simbolos2.erfuncion, yycolumn,yyline,new String(yytext()));}

{comentario} {System.out.println("Comentario: "+yytext()); }


/* CUALQUIER OTRO */
{WhiteSpace}            { /* se ignoran  */}
.                       {this.tokensListErrores.add("Token: "+yytext());
                      System.out.println("Error léxico: "+yytext());}


 