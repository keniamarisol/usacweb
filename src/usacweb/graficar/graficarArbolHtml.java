/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usacweb.graficar;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author id_ma
 */
public class graficarArbolHtml {

    int contador = 0;
    String conexion = "";
    public String graphText = "";

    public void grafico(nodoArbol raiz) {
        graphText += "\ndigraph G {\r\nnode [shape=doublecircle, style=filled, color=pink, fontcolor=black];\n";
        crearNodos(raiz);
        conectarNodos(raiz);
        graphText += "}";

        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter = null;
        try {
            archivo = new FileWriter("C:\\Users\\id_ma\\Documents\\COMPI2\\usacweb\\grafica\\chtml.dot");
            printwriter = new PrintWriter(archivo);
            printwriter.println(graphText);
            printwriter.close();
            //  System.out.println(graphviz);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            String dotPath = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPath = "C:\\Users\\id_ma\\Documents\\COMPI2\\usacweb\\grafica\\chtml.dot";
            String fileOutputPath = "C:\\Users\\id_ma\\Documents\\COMPI2\\usacweb\\grafica\\chtml.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";

            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPath;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;

            Runtime rt = Runtime.getRuntime();

            rt.exec(cmd);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }

    }

    public void conectarNodos(nodoArbol praiz) {

        if (praiz != null) {

            for (nodoArbol temp : praiz.hijos) {

                if (temp != null) {
                    conexion = "\"node" + praiz.contador + "\"->";
                    conexion += "\"node" + temp.contador + "\";";
                    graphText += conexion + "\n";
                    conectarNodos(temp);
                } else {
                    nodoArbol nodo = new nodoArbol("nulo");
                    int cont = praiz.contador + 1;
                    conexion = "\"node" + praiz.contador + "\"->";
                    conexion += "\"node" + cont + "\";";
                    graphText += conexion + "\n";
                    conectarNodos(nodo);
                }
            }

        } else {
            nodoArbol nodo = new nodoArbol("nulo");
            int cont = praiz.contador + 1;
            conexion = "\"node" + praiz.contador + "\"->";
            conexion += "\"node" + cont + "\";";
            graphText += conexion + "\n";
            conectarNodos(nodo);
        }
    }

    public void crearNodos(nodoArbol praiz) {
        graphText += "node" + contador + "[label=\"" + praiz.produccion + "\"];\n";

        if (praiz != null) {
            praiz.contador = contador;
            contador++;
            for (nodoArbol temp : praiz.hijos) {
                if (temp != null) {
                    crearNodos(temp);
                } else {
                    nodoArbol nodo = new nodoArbol("nulo");
                    crearNodos(nodo);
                }
            }

        } else {
            nodoArbol nodo = new nodoArbol("nulo");
            nodo.contador = contador;
            contador++;
            crearNodos(nodo);
        }
    }

}
